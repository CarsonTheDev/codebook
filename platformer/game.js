
let bl, p1, p2, k, plats, score, score1, back
const keys = 'W,A,S,D,UP,DOWN,LEFT,RIGHT,G, R'
const randint = lim => Math.floor(Math.random() * lim)
const rx = () => randint(game.config.width)
const ry = () => randint(game.config.height)

class Main extends Phaser.Scene {

  init() {
    score = 0
    score1 = 0

  }

  preload() {
    this.load.image('bg', './assets/img/backround.png')
    this.load.image('p1', './assets/img/blue_dude.png')
    this.load.image('plats', './assets/img/plat.png')
    this.load.image('p2', './assets/img/red_dude.png')
    this.load.image('coins', './assets/img/coins.png')
    this.load.image('bl', './assets/img/player-player.png')
    this.load.image('over', './assets/img/game.png')

    this.load.audio('coin', './assets/snd/coin.wav')
    this.load.audio('over', './assets/snd/game.wav')
    this.load.audio('back', './assets/snd/bg-music.mp3')
  }
  create() {

    let over = this.sound.add('over')
    back = this.sound.add('back')
    let coin = this.sound.add('coin')
    back.play()

    this.add.image(0, 0, 'bg').setOrigin(0, 0)
    let bl = this.physics.add.sprite(683, 284, 'bl')
    bl.setCollideWorldBounds(true)
    bl.setGravityY(75)
    bl.setGravityY(75)
    bl.setScale(.075)
    bl.setBounce(1)

    p1 = this.physics.add.sprite(100, 100, 'p1')
    p1.setCollideWorldBounds(true)
    p1.setGravityY(600)
    p1.setScale(.075)

    p2 = this.physics.add.sprite(1200, 100, 'p2')

    p2.setCollideWorldBounds(true)
    p2.setGravityY(600)
    p2.setScale(.075)





    let scoreText1 = this.add.text(700, 16, 'score : 0', {
      color: 'grey',
      fontFamily: 'Comic Sans MS',
      fontSize: '30px'
    })

    const gameOver = (p, c) => {
      this.physics.pause()
      back.stop()
      over.play()
      this.add.image(0, 0, 'over').setOrigin(0, 0)
    }

    k = this.input.keyboard.addKeys(keys)

    let scoreText = this.add.text(16, 16, 'score : 0', {
      color: 'grey',
      fontFamily: 'Comic Sans MS',
      fontSize: '30px'
    })

    let coins = this.physics.add.group()
    const spawnCoin = () => coins.create(rx(), ry(), "coins")
    for (let i = 0; i < 10; i++) spawnCoin()


    let plats = this.physics.add.staticGroup()

    plats.create(683, 384, 'plats')
    plats.create(683, 384, 'plats')

    this.physics.add.collider(p1, plats)
    this.physics.add.collider(p2, plats)
    this.physics.add.collider(p1, p2)
    this.physics.add.collider(p2, bl, gameOver)
    this.physics.add.collider(p1, bl, gameOver)
    this.physics.add.collider(plats, bl)
    this.physics.add.collider(coins, plats)

    const collideCoin1 = (p, c) => {
      c.destroy()
      spawnCoin()
      score1 += 1
      scoreText1.setText(`Score: ${score1}`)
      coin.play()
    }


    const collideCoin = (p, c) => {
      c.destroy()
      spawnCoin()
      score += 1
      scoreText.setText(`Score: ${score}`)
      coin.play()
    }
    this.physics.add.collider(p2, coins, collideCoin1)
    this.physics.add.collider(p1, coins, collideCoin)

    bl.setVelocity(400)

  }

  update() {
    if (k.UP.isDown && p1.body.onFloor()) {
      p1.setVelocityY(-600)
    }
    if (k.DOWN.isDown) {
      p1.setVelocityY(-225)
    }
    if (k.RIGHT.isDown) {
      p1.setVelocityX(300)
    }
    if (k.LEFT.isDown) {
      p1.setVelocityX(-300)
    }
    if (p1.body.onFloor()) {
      p1.setDragX(696)
    } else {
      p1.setDragX(100)
    }

    if (k.W.isDown && p2.body.onFloor()) {
      p2.setVelocityY(-600)
    }
    if (k.S.isDown) {
      p2.setVelocityY(-225)
    }
    if (k.D.isDown) {
      p2.setVelocityX(300)
    }
    if (k.A.isDown) {
      p2.setVelocityX(-300)
    }
    if (p2.body.onFloor()) {
      p2.setDragX(696)
    } else {
      p2.setDragX(100)
    }

    if (k.G.isDown) {
      bl.setVelocity(200)
    }
    if (k.R.isDown) {
      back.stop()
      this.scene.restart()

    }
  }
}

let game = new Phaser.Game({

  scene: Main,
  physics: { default: 'arcade' },
  width: 1024,
  length: 768,
  pixelart: true
})