
const keys = 'UP,DOWN,LEFT,RIGHT,SPACE,Q,W,E,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,XC,V,B,N,M'
let p1, p2, k, plats

function preload() {
  this.load.image('bg','./assets/img/backround.png')
  this.load.image('p1','./assets/img/blue_dude.png')  
  this.load.image('plats','./assets/img/platform_platform.png')
  this.load.image('p2','./assets/img/red_dude.png')  
  this.load.image('bl','./assets/img/player-player.png')
  this.load.image('coin','./assets/img/coins.png')
}

function create () {
  this.add.image(0,0,'bg').setOrigin(0,0)
  bl = this.physics.add.sprite(683, 284,'bl')
  p1 = this.physics.add.sprite(100,100,'p1')
  p2 = this.physics.add.sprite(1200,100,'p2')
  coins = this.physics.add.group({
    key: 'coin',
    repeat: 40,
    setXY: {x:10,y:0, stepX: 34}
  }
  )

  coins.children.iterate( child => {
    child.setGravityY(600)
    child.setCollideWorldBounds(true)
  })
  p2.setCollideWorldBounds(true)  
  p1.setCollideWorldBounds(true)  
  bl.setCollideWorldBounds(true)  
  k = this.input.keyboard.addKeys(keys)

  p1.body.bounce = {x:3.5, y:.1}
  p2.body.bounce = {x:3.5, y:.1}
  bl.body.bounce = {x:1, y:1}
  p1.setGravityY(600)
  p2.setGravityY(600)
  bl.setGravityY(75)

  p1.setScale(.075)
  p2.setScale(.075)
  bl.setScale(.075)

  plats = this.physics.add.staticGroup()
  plats.create(683,384,'plats')
  plats.create(683,384,'plats')  
  this.physics.add.collider(p1,plats)
  this.physics.add.collider(p2,plats)
  this.physics.add.collider(p1,p2)   
  this.physics.add.collider(p2,bl)
  this.physics.add.collider(p1,bl)
  this.physics.add.collider(plats,bl)   
  this.physics.add.collider(coins,plats)   

  this.physics.add.collider(coins,p2, (p,c) => {
    c.destroy()
  })
  
  this.physics.add.collider(coins,p1, (p,c) => {
    c.destroy()
  })
  bl.setVelocity(200)
   
}

function update () {
  if (k.UP.isDown && p1.body.onFloor()) {
    p1.setVelocityY(-600)
  }
  if (k.DOWN.isDown) {
    p1.setVelocityY(-225)
  }
  if (k.RIGHT.isDown) {
    p1.setVelocityX(300)
  }
  if (k.LEFT.isDown) {
    p1.setVelocityX(-300)
  }
  if (p1.body.onFloor) {
    p1.setDragX(696)
  }else {
    p1.setDragX(100)
  }

  if (k.W.isDown && p2.body.onFloor()) {
    p2.setVelocityY(-600)
  }
  if (k.S.isDown) {
    p2.setVelocityY(-225)
  }
  if (k.D.isDown) {
    p2.setVelocityX(300)
  }
  if (k.A.isDown) {
    p2.setVelocityX(-300)
  }
  if (p2.body.onFloor) {
    p2.setDragX(696)
  }else {
    p2.setDragX(100)
  }

  if (k.G.isDown) {
    bl.setVelocity(200)
  }
}


let config = {
  height: 768,
  width: 1388, 
  pixelArt: true,
  scene: {preload,create,update},
  physics : {
    default : 'arcade',
    arcade: {
      debug: false
    },

  }
}

new Phaser.Game (config) 